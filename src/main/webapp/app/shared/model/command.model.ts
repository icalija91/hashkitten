import { AttackType } from 'app/shared/model/enumerations/attack-type.model';
import { HashType } from 'app/shared/model/enumerations/hash-type.model';

export class Command {
  constructor(
    public id?: string,
    public hashValue?: string,
    public attackType?: AttackType,
    public hashType?: HashType,
    public useUppercase?: boolean,
    public useLowercase?: boolean,
    public useDigits?: boolean,
    public passwordLength?: number
  ) {
    this.useUppercase = this.useUppercase || false;
    this.useLowercase = this.useLowercase || false;
    this.useDigits = this.useDigits || false;
  }
}
