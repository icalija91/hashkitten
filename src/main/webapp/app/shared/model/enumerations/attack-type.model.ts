export const enum AttackType {
  BRUTE_FORCE = 'BRUTE_FORCE',

  WORD_LIST = 'WORD_LIST',
}
