import { NgModule } from '@angular/core';
import { HashkittenSharedLibsModule } from './shared-libs.module';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';


@NgModule({
  imports: [HashkittenSharedLibsModule],
  declarations: [AlertComponent, AlertErrorComponent],
  exports: [HashkittenSharedLibsModule, AlertComponent, AlertErrorComponent ],
})
export class HashkittenSharedModule {}
