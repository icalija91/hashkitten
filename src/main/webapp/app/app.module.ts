import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { HashkittenSharedModule } from 'app/shared/shared.module';
import { HashkittenCoreModule } from 'app/core/core.module';
import { HashkittenAppRoutingModule } from './app-routing.module';
import { HashkittenHomeModule } from './home/home.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    HashkittenSharedModule,
    HashkittenCoreModule,
    HashkittenHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    HashkittenAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent],
  bootstrap: [MainComponent],
})
export class HashkittenAppModule {}
