import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { Command } from 'app/shared/model/command.model';


@Injectable({ providedIn: 'root' })
export class CommandService {
  public resourceUrl = SERVER_API_URL + 'api/commands';

  constructor(protected http: HttpClient) {}

  create(command: Command): Observable<any> {
    return this.http.post(this.resourceUrl, command, { observe: 'response', responseType: 'text' });
  }
}
