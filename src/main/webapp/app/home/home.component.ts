import { Component, OnInit, OnDestroy } from '@angular/core';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';

import { Command } from 'app/shared/model/command.model';
import { CommandService } from './home.service';



@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  response= "Waiting for requests...";
  isSaving = false;
  aType= "BRUTE_FORCE";
  hType= "SHA1";

  editForm = this.fb.group({
    id: [],
    hashValue: [null, [Validators.required]],
    attackType: [],
    hashType: [],
    useUppercase: [],
    useLowercase: [],
    useDigits: [],
    passwordLength: [],
  });

  constructor(protected commandService: CommandService,  private fb: FormBuilder) {}

  ngOnInit(): void {
  }

  resetForm(): void {
    this.editForm=  this.fb.group({
      id: [],
      hashValue: [null, [Validators.required]],
      attackType: [ this.editForm.get(['attackType'])!.value],
      hashType: [this.editForm.get(['hashType'])!.value],
      useUppercase: [],
      useLowercase: [],
      useDigits: [],
      passwordLength: [],
    });
  }

  execute(): void {
    this.isSaving = true;
    this.response= "Executing command...";
    const command = this.createFromForm();
      this.commandService.create(command).subscribe(result =>  {
        this.response=result.body
        this.isSaving = false;
        this.resetForm();
      })

  }

  private createFromForm(): Command {
    return {
      ...new Command(),
      id: this.editForm.get(['id'])!.value,
      hashValue: this.editForm.get(['hashValue'])!.value,
      attackType: this.editForm.get(['attackType'])!.value,
      hashType: this.editForm.get(['hashType'])!.value,
      useUppercase: this.editForm.get(['useUppercase'])!.value,
      useLowercase: this.editForm.get(['useLowercase'])!.value,
      useDigits: this.editForm.get(['useDigits'])!.value,
      passwordLength: this.editForm.get(['passwordLength'])!.value,
    };
  }

  ngOnDestroy(): void {
  }
}
