package com.digitalna.forenzika.hashcat.domain.enumeration;

/**
 * The HashType enumeration.
 */
public enum HashType {
    MD5, SHA1
}
