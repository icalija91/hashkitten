package com.digitalna.forenzika.hashcat.domain.enumeration;

/**
 * The AttackType enumeration.
 */
public enum AttackType {
    BRUTE_FORCE, WORD_LIST
}
