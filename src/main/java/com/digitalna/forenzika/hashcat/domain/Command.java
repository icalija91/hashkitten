package com.digitalna.forenzika.hashcat.domain;

import javax.validation.constraints.*;

import java.io.Serializable;

import com.digitalna.forenzika.hashcat.domain.enumeration.AttackType;

import com.digitalna.forenzika.hashcat.domain.enumeration.HashType;


public class Command implements Serializable {

    @NotNull
    private String hashValue;

    @NotNull
    private AttackType attackType;

    @NotNull
    private HashType hashType;

    private boolean useUppercase;

    private boolean useLowercase;

    private boolean useDigits;

    private Long passwordLength;

    public String getHashValue() {
        return hashValue;
    }

    public Command hashValue(String hashValue) {
        this.hashValue = hashValue;
        return this;
    }

    public void setHashValue(String hashValue) {
        this.hashValue = hashValue;
    }

    public AttackType getAttackType() {
        return attackType;
    }

    public Command attackType(AttackType attackType) {
        this.attackType = attackType;
        return this;
    }

    public void setAttackType(AttackType attackType) {
        this.attackType = attackType;
    }

    public HashType getHashType() {
        return hashType;
    }

    public Command hashType(HashType hashType) {
        this.hashType = hashType;
        return this;
    }

    public void setHashType(HashType hashType) {
        this.hashType = hashType;
    }

    public boolean isUseUppercase() {
        return useUppercase;
    }

    public Command useUppercase(boolean useUppercase) {
        this.useUppercase = useUppercase;
        return this;
    }

    public void setUseUppercase(boolean useUppercase) {
        this.useUppercase = useUppercase;
    }

    public boolean isUseLowercase() {
        return useLowercase;
    }

    public Command useLowercase(boolean useLowercase) {
        this.useLowercase = useLowercase;
        return this;
    }

    public void setUseLowercase(boolean useLowercase) {
        this.useLowercase = useLowercase;
    }

    public boolean isUseDigits() {
        return useDigits;
    }

    public Command useDigits(boolean useDigits) {
        this.useDigits = useDigits;
        return this;
    }

    public void setUseDigits(boolean useDigits) {
        this.useDigits = useDigits;
    }

    public Long getPasswordLength() {
        return passwordLength;
    }

    public Command passwordLength(Long passwordLength) {
        this.passwordLength = passwordLength;
        return this;
    }

    public void setPasswordLength(Long passwordLength) {
        this.passwordLength = passwordLength;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here


    // prettier-ignore
    @Override
    public String toString() {
        return "Command{" +
            "hashValue='" + getHashValue() + "'" +
            ", attackType='" + getAttackType() + "'" +
            ", hashType='" + getHashType() + "'" +
            ", useUppercase='" + isUseUppercase() + "'" +
            ", useLowercase='" + isUseLowercase() + "'" +
            ", useDigits='" + isUseDigits() + "'" +
            ", passwordLength=" + getPasswordLength() +
            "}";
    }
}
