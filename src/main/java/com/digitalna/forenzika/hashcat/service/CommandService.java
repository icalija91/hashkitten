package com.digitalna.forenzika.hashcat.service;

import com.digitalna.forenzika.hashcat.domain.Command;
import com.digitalna.forenzika.hashcat.domain.enumeration.AttackType;
import com.digitalna.forenzika.hashcat.util.HashCatCommandGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service
public class CommandService {

    private final Logger log = LoggerFactory.getLogger(CommandService.class);

    public CommandService() {}

    public String execute(Command command) {
        log.debug("Executing command : {}", command);
        String result;
        String hashType= HashCatCommandGenerator.getHashType(command.getHashType().name());

        if(AttackType.WORD_LIST.equals(command.getAttackType())){
            result = HashCatCommandGenerator.wordlistAttack(hashType,command.getHashValue());
        } else {
            String customCharset = HashCatCommandGenerator.generateCustomCharset(command);
            result = HashCatCommandGenerator.bruteForceAttack(hashType,customCharset,command.getPasswordLength(),command.getHashValue());
        }

        return result;
    }
}
