package com.digitalna.forenzika.hashcat.util;

import com.digitalna.forenzika.hashcat.domain.Command;

import static com.digitalna.forenzika.hashcat.util.CommandLineUtil.executeCommand;

public class HashCatCommandGenerator {

    private static final String HASHCAT_PATH="D:/Programs/hashcat-6.1.1/hashcat.bat";
    private static final String PASSWORD_LIST="passwordList.txt";
    private static final String UPPER_CASE="?u";
    private static final String LOWER_CASE="?l";
    private static final String DIGITS="?d";

    public static String getHashType (String ht){
        String hashType = null;

        switch (ht) {
            case "MD5":  hashType = "0";
                break;
            case "SHA1":  hashType = "100";
                break;
        }

        return hashType;
    }

    public static String wordlistAttack(String hashType, String hashValue){
        String command= HASHCAT_PATH + " "  + "-m" + hashType + " " + hashValue + " " + PASSWORD_LIST;

        return CommandLineUtil.executeCommand(command);
    }

    public static String generateCustomCharset(Command c){
        String customCharset = "-1 ";
        if(c.isUseUppercase()){
            customCharset+=UPPER_CASE;
        }  if (c.isUseLowercase()){
            customCharset+=LOWER_CASE;
        }  if (c.isUseDigits()){
            customCharset+=DIGITS;
        }
        return customCharset;
    }

    public static String bruteForceAttack (String hashType, String customCharset, Long passwordLength, String hashValue){

        String command = HASHCAT_PATH + " "  + "-m" + hashType + " -a3 "+ hashValue + " " + customCharset + " ";

        //create mask based off password length
        for(int i=0;i<passwordLength;i++){
            command = command + "?1";
        }

        return CommandLineUtil.executeCommand(command);
    }

}
