package com.digitalna.forenzika.hashcat.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandLineUtil {

    public static String executeCommand(String command){

        String line = "";
        String all = "";

        try {
            Process p = Runtime.getRuntime().exec(command); //execute the command in terminal

            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream())); //  reads a line from terminal

            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            // Read line by line the output of hashcat and save everything in one big string
            while ((line = stdInput.readLine()) != null) {
                System.out.println(line);
                all = all + "\n" + line;
            }

            //If there is an error, read line by line and return everything
            while ((line = stdError.readLine()) != null) {
                System.out.println(line);
                all = all + "\n" + line;
            }


        }
        catch (IOException e) {
            System.out.println("----ATENTION----:ERROR:  ");
            System.exit(-1);
        }
        return all;
    }
}
