package com.digitalna.forenzika.hashcat.web.rest;

import com.digitalna.forenzika.hashcat.domain.Command;
import com.digitalna.forenzika.hashcat.service.CommandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;


@RestController
@RequestMapping("/api")
public class CommandResource {

    private final Logger log = LoggerFactory.getLogger(CommandResource.class);

    private final CommandService commandService;

    public CommandResource(CommandService commandService) {
        this.commandService = commandService;
    }

    @PostMapping("/commands")
    public ResponseEntity<String> createCommand(@Valid @RequestBody Command command)  {
        log.debug("REST request to execute Command : {}", command);

        String crackedPassword = commandService.execute(command);
        return ResponseEntity.ok()
            .body(crackedPassword);
    }
}
